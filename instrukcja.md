# Lekcja 1: Wprowadzenie do Git Bash

Witaj w lekcji 1! W tej lekcji nauczysz się podstaw korzystania z Git Bash.

## Instalacja Git Bash

Aby zacząć, zainstaluj Git Bash na swoim komputerze.

## Utworzenie nowego repozytorium

1. Otwórz terminal Git Bash.
2. Przejdź do katalogu, w którym chcesz utworzyć repozytorium.
3. Utwórz nowe repozytorium: `git init`.

## Tworzenie pliku instrukcja.md

1. Utwórz plik instrukcja.md: `touch instrukcja.md`.
2. Otwórz plik w edytorze tekstu: `nano instrukcja.md`.
3. Dodaj treść rozdziału Lekcja 1 w formacie Markdown.

...
